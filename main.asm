%include 'lib.inc'
%include 'dict.inc'
%include 'words.inc'

%define INPUT_BUFFER_SIZE 256


section .rodata
buffer_overflow_error_msg: db 'Buffer overflow', 0
key_not_found_error_msg: db 'Key not found', 0

section .bss
input_buffer: resb INPUT_BUFFER_SIZE

section .text
global _start

_start:
    mov rdi, input_buffer
    mov rsi, INPUT_BUFFER_SIZE
    call read_word
    test rax, rax               ; если rax == 0, то буфер переполнен
    jz .buffer_overflow
    mov rdi, input_buffer       ; введённый ключ - 1ый аргумент
    mov rsi, TOP_NODE           ; адрес первого элемента словаря - 2ой аргумент
    call find_word
    test rax, rax               ; если rax == 0, то ключ не найден
    jz .key_not_found   
    add rax, 8                  ; смещение адреса к адресу ключа элемента
    mov rdi, rax                ; 1-ый аргумент - адрес ключа
    mov rcx, rax                ; тут просто бэкапим rax в rcx
    call string_length          ; находим ещё одно смещение - к адресу значения элемента
    add rax, rcx                ; смещаясь ещё на длину ключа, мы подойдём к адресу значения
    inc rax                     ; учитываем, что string_length не подсчитывает нуль-терминал
    mov rdi, rax                ; указатель на найденное по ключу значение в словаре - 1ый аргумент
    call print_string
    xor rdi, rdi                ; перед выходом ставим успешный код возврата
    jmp .end
    .buffer_overflow:
        mov rdi, buffer_overflow_error_msg
        jmp .error
    .key_not_found:
        mov rdi, key_not_found_error_msg
    .error:
        call print_error        ; выводи ранее переданное сообщение об ошибке
        mov rdi, 1              ; ставим ошибочный код возврата
    .end:
        call exit
